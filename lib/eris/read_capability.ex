# SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
# SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERIS.ReadCapability do
  @moduledoc """
  ERIS read capability and encoding of read capability as binary and URN.
  """

  defstruct block_size: nil, level: nil, reference: nil, key: nil

  @type t :: %__MODULE__{
          block_size: number,
          level: number,
          reference: binary,
          key: binary
        }

  @doc """
  Encode ERIS read capability `read_capability` as binary.

  See http://purl.org/eris#_binary_encoding_of_read_capability
  """
  @spec to_binary(t) :: binary
  def to_binary(%__MODULE__{} = read_capability) do
    case read_capability.block_size do
      1024 ->
        <<0>> <> <<read_capability.level>> <> read_capability.reference <> read_capability.key

      32_768 ->
        <<1>> <> <<read_capability.level>> <> read_capability.reference <> read_capability.key
    end
  end

  @doc """
  Encode ERIS read capability `read_capability` as URN and return as string.

  See http://purl.org/eris#_urn
  """
  @spec to_string(t) :: String.t()
  def to_string(%__MODULE__{} = read_capability) do
    "urn:erisx2:" <>
      (read_capability
       |> to_binary()
       |> Base.encode32(padding: false))
  end

  @doc """
  Parse an ERIS read capability from a URN (string representaiton) or binary encoding.
  """
  @spec parse(any) :: {:ok, t} | {:error, atom}
  def parse(%__MODULE__{} = read_capability), do: {:ok, read_capability}

  def parse(urn) when is_binary(urn) do
    case urn do
      "urn:erisx2:" <> base32_encoded ->
        base32_encoded
        |> Base.decode32(padding: false)
        |> parse

      <<0>> <> <<level>> <> <<reference::binary-size(32)>> <> <<key::binary-size(32)>> ->
        {:ok, %__MODULE__{block_size: 1024, level: level, reference: reference, key: key}}

      <<1>> <> <<level>> <> <<reference::binary-size(32)>> <> <<key::binary-size(32)>> ->
        {:ok, %__MODULE__{block_size: 32_768, level: level, reference: reference, key: key}}

      _ ->
        {:error, :invalid_read_capability}
    end
  end

  # handle return from `Base.decode32`
  def parse({:ok, urn}), do: parse(urn)

  def parse(_), do: {:error, :invalid_read_capability}

  defimpl Inspect, for: ERIS.ReadCapability do
    def inspect(read_capability, _opts) do
      "~ERIS<#{ERIS.ReadCapability.to_string(read_capability)}>"
    end
  end
end
