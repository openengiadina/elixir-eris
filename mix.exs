# SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
#
# SPDX-License-Identifier: CC0-1.0

defmodule ERIS.MixProject do
  use Mix.Project

  def project do
    [
      app: :eris,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:monocypher,
       git: "https://gitlab.com/openengiadina/erlang-monocypher.git", branch: "main"},
      {:jason, "~> 1.2", only: :test}
    ]
  end
end
