# SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERISLargeTest do
  use ExUnit.Case
  doctest ERIS.Encode

  def chacha20_stream(test_name, block_size) do
    with key <- :monocypher.crypto_blake2b_general(32, <<>>, test_name),
         block_size_bits <- block_size * 8,
         null_block <- <<0::size(block_size_bits)>>,
         null_nonce <- <<0::size(96)>> do
      Stream.resource(
        fn -> 0 end,
        fn counter ->
          {[:monocypher.crypto_ietf_chacha20_ctr(null_block, key, null_nonce, counter)],
           counter + div(block_size, 64)}
        end,
        fn _ -> :ok end
      )
    end
  end

  def is_read_capability(%ERIS.ReadCapability{}), do: true
  def is_read_capability(_), do: false

  test "100MiB (block size 1KiB)" do
    assert "urn:erisx2:AACXPZNDNXFLO4IOMF6VIV2ZETGUJEUU7GN4AHPWNKEN6KJMCNP6YNUMVW2SCGZUJ4L3FHIXVECRZQ3QSBOTYPGXHN2WRBMB27NXDTAP24" =
             Stream.take(chacha20_stream("100MiB (block size 1KiB)", 1024), 1024 * 100)
             |> ERIS.Encode.stream_encode(block_size: 1024, convergence_secret: <<0::256>>)
             |> Stream.filter(&is_read_capability/1)
             |> Enum.map(&ERIS.ReadCapability.to_string/1)
             |> List.first()
  end

  test "1GiB (block size 32KiB)" do
    assert "urn:erisx2:AEBFG37LU5BM5N3LXNPNMGAOQPZ5QTJAV22XEMX3EMSAMTP7EWOSD2I7AGEEQCTEKDQX7WCKGM6KQ5ALY5XJC4LMOYQPB2ZAFTBNDB6FAA" =
             Stream.take(
               chacha20_stream("1GiB (block size 32KiB)", 32_768),
               div(1024 * 1024 * 1024, 32_768)
             )
             |> ERIS.Encode.stream_encode(block_size: 32_768, convergence_secret: <<0::256>>)
             |> Stream.filter(&is_read_capability/1)
             |> Enum.map(&ERIS.ReadCapability.to_string/1)
             |> List.first()
  end

  @tag timeout: :infinity
  @tag :skip
  test "256GiB (block size 32KiB)" do
    assert "urn:erisx2:AEBZHI55XJYINGLXWKJKZHBIXN6RSNDU233CY3ELFSTQNSVITBSVXGVGBKBCS4P4M5VSAUOZSMVAEC2VDFQTI5SEYVX4DN53FTJENWX4KU" =
             Stream.take(
               chacha20_stream("256GiB (block size 32KiB)", 32_768),
               div(1024 * 1024 * 1024 * 256, 32_768)
             )
             |> ERIS.Encode.stream_encode(block_size: 32_768, convergence_secret: <<0::256>>)
             |> Stream.filter(&is_read_capability/1)
             |> Enum.map(&ERIS.ReadCapability.to_string/1)
             |> List.first()
  end
end
